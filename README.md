# Running
Simply run `./start_simulator` in the main directory.
This adds pymunk and pygame to the PYTHONPATH and adds chipmunk to the LD_LIBRARY_DIR then runs Simulator.py

# Using as a library
The simulator can also be run as a library; simply instantiate Simulator.Simulation.Simulation and run its "start" method. This will spawn the simulation as a subprocess.

# Design
## Model
The model deals with the meat of the work in this simulator, running a physics simulation and calling the drawing methods. It makes use of pymunk (a Python wrapper for Chipmunk) for physics simulations (I couldn't be arsed to write collision detection; that stuff is *tricky*) which makes it fairly speedy.

### Ball
The ball is a simple circle mass with high elasticity, low mass, and low friction.

### Robot
A robot is a rectangular body with wheels attached as sub-bodies using PivotJoints. This means that to move the robot you should move its wheels, though for mouse dragging I would move the robot itself (which would bring the wheels along)
The robot's kicker is a small, high-mass, rectangle attached on a very stiff spring with two GrooveJoints to ensure it can only move forwards and to ensure it cannot extend too far.

### Pitch
The pitch is currently entirely surrounded by walls, though I wish to add goals (with their own walls) at a later date.

## Drawing
This simply handles the process of drawing the objects to a pygame surface. This should be obsolete when the simulator has a vision server attached; meaning we can run it headlessly (on the compute server, for instance) and connect one of the drawing systems from strategy to it.

## Servers
The servers deal with sending data out to a strategy and receiving commands back from strategy

# Future
The simulator seems to be roughly feature complete to me now. It could do with fine-tuning of parameters to make it realistic though.

Perhaps it would be useful for the simulation to be able to directly connect to a vision server and keyframe itself based upon that server. This would include estimating velocities and robot motions. The ultimate goal being to make a VisionSource in strategy that gets its information from a simulation which is keyframed on the real vision. This would allow the strategy to have a roughly accurate real-time response to the world rather than the somewhat delayed response we would have without.

Another possibly useful capability would be updating parameters on the fly. I have some ideas on this that I hope to soon put into action.
