""" An executable that runs the simulation as a drop-in replacement for the real-world pitch and robots """
from math import pi
from optparse import OptionParser

import pymunk
import pygame
from twisted.internet import reactor

from Simulator.Drawing.Drawing import green
from Simulator.Model.WorldObjects import Ball, Robot, Pitch
from Simulator.Params import Params
from Simulator.Simulation import Simulation
from Simulator.Servers.VisionServer import VisionServerStarter
from Simulator.Servers.CommsServer import CommsServerFactory

(w, h) = Params.pitchSize

objects = {}
objects['ball'] = Ball((w/2, h/2), (0, 0))

objects['blue'] = Robot((60, h/2), (0,0), 0, "blue")

objects['yellow'] = Robot((w-60, h/2), (0,0), pi, "yellow")

simulation = Simulation(objects, draw=True)
vss = VisionServerStarter(simulation)
vss.start(31410)
csf = CommsServerFactory(simulation)
csf.start()

simulation.start()

while not simulation.done.value:
    pass
